#!/bin/sh -e

COMPRESS=xz

ONAME=#SOFTWARENAME#
GITHUBURL=https://github.com/#GITHUBTEAM#/${ONAME}

NAME=`dpkg-parsechangelog | awk '/^Source/ { print $2 }'`
MVERSION=`dpkg-parsechangelog | awk '/^Version:/ { print $2 }' | sed -e 's/+dfsg//' -e 's/^\([0-9][0-9\.a-z]\+\)[+~][-0-9]\+$/\1/'`

COPYRIGHT=$(pwd)/debian/copyright

mkdir -p ../tarballs
cd ../tarballs
# need to clean up the tarballs dir first because upstream tarball might
# contain a directory with unpredictable name
rm -rf *
git clone --quiet $GITHUBURL
cd ${ONAME}
VERSION=${MVERSION}+`date -d @$(git show --format="%at" | head -n1) +%Y%m%d`+dfsg
# for esthetical reasons set file timestamps (if git-restore-mtime is installed)
git restore-mtime || true
cd ..
TARDIR=${NAME}-${VERSION}
mv ${ONAME} ${TARDIR}
rm -rf ${TARDIR}/.git
# Remove according to "Files-Excluded"
# Its a bit tricky since '*' needs to be escaped ...
for excl in $(grep "^Files-Excluded" ${COPYRIGHT} | sed -e 's/^Files-Excluded: */ /' -e 's/ \*/ \\*/g') ; do find . -path "$(echo $excl | sed 's/^\\//')" | xargs rm -rf ; done

GZIP="--best --no-name" tar --owner=root --group=root --mode=a+rX -caf "$NAME"_"$VERSION".orig.tar.${COMPRESS} "${TARDIR}"
rm -rf ${TARDIR}
